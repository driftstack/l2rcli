from bake_modules.variable import Variable
import unittest
import yaml

class TestVariableBakeModule(unittest.TestCase):

    def test_variable(self):

        yamlData = """
vars:
    var1: test
    var2:
tasks:
  - name: hello
    command: echo hi
"""
        
        data = yaml.load(yamlData, Loader=yaml.SafeLoader)
        vars = Variable(data["vars"], {'var2': 'test2'})
        self.assertEqual({'var1': 'test', 'var2': 'test2'}, vars.toDict())

