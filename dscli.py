import argparse
from bake_command import BakeCommand
from port_command import PortCommand
from port_delete_command import PortDeleteCommand
from port_list_command import PortListCommand
from port_update_command import PortUpdateCommand
from key_value_parser import KeyValueParser
from credential_helper import CredentialHelper
from tabulate import tabulate
from pathlib import Path
import os

parser = argparse.ArgumentParser(description='DriftStack CLI')
subparsers = parser.add_subparsers(help="commands", dest="command")


bake_subparser = subparsers.add_parser("bake", help="Bake a recipe repo")

bake_subparser.add_argument("path",
    type=str,
    help="Path to recipe in repo or file")
bake_subparser.add_argument("-a","--answer",
                        nargs='*',
                        required=False,
                        action=KeyValueParser,
                        metavar="KEY=VALUE",
                        help="Add key/value params. May appear multiple times. Aggregate in dict")


bake_subparser.add_argument("--repo",
    type=str,
    help="Recipe repo")
bake_subparser.add_argument('--api-logs', action='store_true', help="Push logs to api")

port_parser = subparsers.add_parser("port", help="Allow port access to workspace")

port_subparser = port_parser.add_subparsers(help='Port command.', dest="port_command")

# Update Port
port_update_parser = port_subparser.add_parser("update", help="Update port for workspace.")
port_update_parser.add_argument('port',
    type=str,
    help="Port to update")
port_update_parser.add_argument('--share-token', action='store_true', help="Add share token")
port_update_parser.add_argument('--public', action='store_true', help="Allow public access")
port_update_parser.add_argument('--internal', action='store_true', help="Allow internal team access")
port_update_parser.add_argument('--private', action='store_true', help="Allow only your user and any share token users can access")

# Add Port
port_add_parser = port_subparser.add_parser("add", help="Add port to workspace.")
port_add_parser.add_argument('port',
    type=str,
    help="Port to open on workspace")
port_add_parser.add_argument('--public', action='store_true', help="Allow public access")
port_add_parser.add_argument('--internal', action='store_true', help="Allow internal team access")
port_add_parser.add_argument('--share-token', action='store_true', help="Add share token")
port_add_parser.add_argument("-n", "--name",
    type=str,
    help="Sub name for the domain")

# Delete Port
port_delete_parser = port_subparser.add_parser("delete", help="Delete port.")
port_delete_parser.add_argument('port',
    type=str,
    help="Port to delete")

# List Ports
port_list_parser = port_subparser.add_parser("list", help="List ports")
port_list_parser.add_argument('-a', '--all', action='store_true')


credential_helper = subparsers.add_parser("credential-helper", help="Credential Helper")
credential_helper_subparser = credential_helper.add_subparsers(help='Credential Helper', dest="credential_helper")
credential_helper_subparser.add_parser("store", help="Store git credentials.")
credential_helper_subparser.add_parser("get", help="Get git credentials.")
credential_helper_subparser.add_parser("erase", help="Erase git credentials.")


args = parser.parse_args()

if args.command == "bake":
   bake = BakeCommand(args.repo, args.path, args.answer, args.api_logs)
   bake.run()

if 'port_command' in args and args.port_command == "add":
    port = args.port
    public = args.public
    internal = args.internal
    shareToken = args.share_token
    name = args.name
    portCommand = PortCommand(port, public, internal, name, shareToken)
    portCommand.run()

if 'port_command' in args and args.port_command == "update":
    port = args.port
    shareToken = args.share_token
    internal = args.internal
    public = args.public
    private = args.private
    portCommand = PortUpdateCommand(port, shareToken, public, internal, private)
    portCommand.run()

if 'port_command' in args and args.port_command == "delete":
    port = args.port
    portCommand = PortDeleteCommand(port)
    portCommand.run()
    
if 'port_command' in args and args.port_command == "list":
    portListCommand = PortListCommand(args.all)
    portListCommand.run()

if 'credential_helper' in args and args.credential_helper == "store":
    CredentialHelper().store()
if 'credential_helper' in args and args.credential_helper == "get":
    CredentialHelper().get()
if 'credential_helper' in args and args.credential_helper == "erase":
    CredentialHelper().erase()

