import subprocess
import os
import hashlib
from pathlib import Path
import yaml
from port_command import PortCommand
from jinja2 import Environment, BaseLoader
from bake_modules.variable import Variable
import glob
from systemd import create_service
from config import Config
import requests


home = str(Path.home())

class BakeCommand:
    def __init__(self, repo, path, answers={}, apiLogs=False):
        self.repo = repo
        self.path = path
        self.answers = answers
        self.apiLogs = apiLogs
        if self.repo is not None:
            self.repoHash = hashlib.md5(repo.encode()).hexdigest()
            self.repoPath = home+os.path.sep+".l2rcli"+os.path.sep+"repos"+os.path.sep+self.repoHash

    
    def run(self):
        try:
            if self.repo is not None:
                if os.path.isdir(self.repoPath):
                    subprocess.check_output(["git", "-C", self.repoPath,  "pull"])
                else:
                    subprocess.check_output(["git",  "clone", self.repo, self.repoPath])
        except subprocess.CalledProcessError as e:
            print(e.output)
        
        if self.repo is not None:
            return self.__runTasks(self.repoPath+os.path.sep+self.path+".yml")

        return self.__runTasks(self.path)
    def __runTasks(self, filePath):
        file = open(filePath)
        data = yaml.load(file, Loader=yaml.SafeLoader)
        varsData = {}
        if 'vars' in data:
            varsData = Variable(data['vars'], self.answers).toDict()
        for task in data['tasks']:
            try:
                title='--- '+task['name']+' ---'
                self.print(title)
                if 'include' in task:
                    path = self.renderValue(task['include']['path'], varsData)
                    if 'repo' not in task['include']:
                        path = glob.glob(self.renderValue(task['include']['path'], varsData))[0]
                    repo = self.renderValue(task['include']['repo'], varsData) if 'repo' in task['include'] else None
                    include = BakeCommand(repo, path)
                    include.run()
                if 'command' in task:
                    cmd = subprocess.Popen(self.renderValue(task['command'], varsData),  stdout=subprocess.PIPE,  stderr=subprocess.STDOUT, shell=True)
                    for line in iter(cmd.stdout.readline, ''):
                        if line.decode() == '':
                            break
                        self.print(line.decode('utf-8'), end='')
                    out, err = cmd.communicate() 
                    if cmd.returncode != 0:
                        return
                if 'openport' in task:
                    opTask = task['openport']
                    name = self.renderValue(opTask['name'], varsData) if 'name' in opTask else None
                    port = self.renderValue(opTask['port'], varsData) 
                    public = self.renderValue(opTask['public'], varsData) if 'public' in opTask else False
                    internal = self.renderValue(opTask['internal'], varsData) if 'internal' in opTask else False
                    plc = PortCommand(port,public,internal,name)
                    plc.run()
                    self.print(plc.message)
                    self.print(plc.url)
                if 'service' in task:
                    user = 'root'
                    working_dir = None
                    if 'user' in task['service']:
                        user = task['service']['user']
                    if 'working_dir' in task['service']:
                        working_dir = task['service']['working_dir']
                    create_service(task['service']['command'], task['service']['name'], working_dir, user)
                    
            except subprocess.CalledProcessError as e:
                self.print(e.output)
                exit(120)
    def renderValue(self, string, vars):
        rtemplate = Environment(loader=BaseLoader).from_string(str(string))
        return rtemplate.render(vars)

    def print(self, text, end=os.linesep):
        if self.apiLogs:
            config = Config()
            url = config.get('api_url')+'instance-utilities/logs/'+config.get("uid")
            r = requests.post(url, json={
                    "data": text
                },
                headers={
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+config.get('token')
                })
        print(text, end=end)
