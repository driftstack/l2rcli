import requests
from config import Config
import json

class PortDeleteCommand:
    def __init__(self, port):
        self.port = port
        self.config = Config()

    def run(self):
        url = self.config.get('api_url')+'instance-ports'
        r = requests.delete(url, json={
            "uid": self.config.get('uid'), 
            "port": self.port
            },
            headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+self.config.get('token')
            })
        print(r.json()['message'])